#!/bin/bash
sudo yum update -y
sudo amazon-linux-extras install docker -y
sudo service docker start
sudo curl -L https://github.com/docker/compose/releases/latest/download/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
sudo yum install git -y
git clone https://gitlab.com/shubhamias12/test.git
sudo cd test/
sudo cd monitor
sudo chmod 666 /var/run/docker.sock
docker-compose up -d
