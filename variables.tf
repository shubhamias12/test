variable "public_key" {
  default = "tests"
}

variable "private_key" {
   default = "tests2"
}

variable "key_name" {
  default = "tests"
  description = "Desired name of AWS key pair"
}

variable "aws_region" {
  default = "us-east-1"
}

variable "vpc_cidr" {
  default = "10.0.0.0/16"
}

variable "subnet_cidr" {
  default = "10.0.1.0/24"
}

variable "ami" {
  default = {
    eu-west-1 = "ami-0ea3405d2d2522162"
    us-east-1 = "ami-09d95fab7fff3776c"
  }
}

variable "instancetype" {
  default = "t2.micro"
}

variable "master_count" {
  default = 1
}

// variable "private_key"{
//   type = string
// }
