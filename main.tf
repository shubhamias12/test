terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

provider "aws" {
  region     = "${var.aws_region}"
}

resource "aws_vpc" "demovpc" {
  cidr_block       = "${var.vpc_cidr}"
  instance_tenancy = "default"

  tags = {
    Name = "Demo"
  }
}


resource "aws_internet_gateway" "demogateway" {
  vpc_id = "${aws_vpc.demovpc.id}"
  tags = {
      Name = "Demo"
  }
}

resource "aws_route" "internet_access" {
  route_table_id         = "${aws_vpc.demovpc.main_route_table_id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = "${aws_internet_gateway.demogateway.id}"
}


resource "aws_subnet" "demosubnet" {
  vpc_id                  = "${aws_vpc.demovpc.id}"
  cidr_block             = "${var.subnet_cidr}"
  map_public_ip_on_launch = true
  availability_zone = "us-east-1a"

  tags = {
    Name = "Demo"
  }
}

resource "aws_security_group" "demosg" {
  name        = "Demo Security Group"
  description = "Demo Module"
  vpc_id      = "${aws_vpc.demovpc.id}"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
      Name = "Demo"
  }
}

resource "aws_key_pair" "demokey" {
  key_name   = "${var.key_name}"
  public_key = "${file(var.public_key)}"

  tags = {
      Name = "Demo"
  }
}
  
resource "aws_instance" "demoinstance" {

  ami = "${lookup(var.ami, var.aws_region)}"

  subnet_id = "${aws_subnet.demosubnet.id}"

  instance_type = "${var.instancetype}"
  
  #count= "${var.master_count}"
  
  key_name = "${aws_key_pair.demokey.id}"

  vpc_security_group_ids = ["${aws_security_group.demosg.id}"]

  tags = {
    Name = "Demo"
  }

  user_data = "${file("install_docker.sh")}"
}
//   user_data = <<EOF
// #!/bin/bash
// sudo yum update -y
// sudo amazon-linux-extras install docker -y
// sudo service docker start
// sudo curl -L https://github.com/docker/compose/releases/latest/download/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
// sudo chmod +x /usr/local/bin/docker-compose
// sudo yum install git -y
// sudo git clone https://gitlab.com/shubhamias12/test.git
// sudo cd test/
// sudo cd monitor
// sudo chmod 666 /var/run/docker.sock
// docker-compose up -d
// EOF
// }

# */
# resource "null_resource" "copy_html"{

  
#   connection {
#     host = aws_instance.demoinstance.public_ip
#     type = "ssh"
#     user = "ec2_user"
#     #private_key = "${file(var.private_key)}"
#     private_key = "${file("/home/cloudshell-user/test")}"
#     #agent = true
#   }  
#   provisioner "file" {
#     source      = "monitor"
#     destination = "/home/ec2_user/"
#   }

#   depends_on = [aws_instance.demoinstance]
# }

  
